.PHONY: tests
tests:
	docker-compose run --rm api sh -c "python manage.py test"

.PHONY: lint
lint:
	docker-compose run --rm api sh -c "flake8"

.PHONY: up
up:
	docker-compose up

.PHONY: down
down:
	docker-compose down

.PHONY: build
build:
	docker-compose build

.PHONY: migration
migration:
	docker-compose run --rm api sh -c "python manage.py makemigrations"

.PHONY: migrate
migrate:
	docker-compose run --rm api sh -c "python manage.py wait_for_db && python manage.py migrate"

.PHONY: createsuperuser
createsuperuser:
	docker-compose run --rm api sh -c "python manage.py createsuperuser"

.PHONY: run-deploy
run-deploy:
	docker-compose -f docker-compose-deploy.yml up